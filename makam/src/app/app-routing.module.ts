import { AnnonceDetailComponent } from './components/mobile/annonce-detail/annonce-detail.component';
import { ProfileComponent } from './components/auth/profile/profile.component';
import { SearchResultComponent } from './components/mobile/search-result/search-result.component';
import { HomeComponent } from './components/mobile/home/home.component';
import { ResetPasswordComponent } from './components/auth/reset-password/reset-password.component';
import { ForgotPasswordComponent } from './components/auth/forgot-password/forgot-password.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { LoginComponent } from './components/auth/login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path:"login",component:LoginComponent},
  {path:"register",component:RegisterComponent},
  {path:"password-forgot",component:ForgotPasswordComponent},
  {path:"reset-password",component: ResetPasswordComponent},
  {path:"home",component:HomeComponent},
  {path:"search-result/:city/:type",component:SearchResultComponent},
  {path:"search-result/:id",component:AnnonceDetailComponent},
  {path:"profile",component:ProfileComponent},
  {path:"",redirectTo:'home',pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
