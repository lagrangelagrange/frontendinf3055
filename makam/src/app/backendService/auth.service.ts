import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private _http: HttpClient)  { 
  }
  public register(body:any){
   return this._http.post('https://tpoop-production.up.railway.app/account/register/',body);
  }
  public login(body:any){
    return this._http.post('https://tpoop-production.up.railway.app/account/login/',body);
  }
}
