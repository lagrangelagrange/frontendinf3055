import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AnnonceService {

  constructor(
    private _http: HttpClient
  ) { }

  public getSingleAnnounce(id:number){
    return this._http.get('https://tpoop-production.up.railway.app/house/' + id);
  }
}
