import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  city = 'Yaounde';
  type = '';
  constructor(
    private _router: Router
  ) { }

  ngOnInit(): void {
    
  }

  Search(){
    this._router.navigate(['/search-result/',this.city,this.type])
  }
  Select_ChambreMod(){
    this.type = 'chambre_modern'
  }
  SelectChambreSim(){
    this.type = 'chambre_simple'
  }
  SelectStudioMod(){
    this.type = 'studio_modern'
  }
  SelectMiNuStudio(){
    this.type = 'mi_nu_studio'
  }
  Select_Appart(){
    this.type = 'appartement'
  }
  SelectAppartMeub(){
    this.type = 'appartement_meubler'
  }

  //CITY SELECTION
  selectYaounde(){
    this.city = 'Yaounde'
  }
  selectDouala(){
    this.city = 'Douala'
  }
}
