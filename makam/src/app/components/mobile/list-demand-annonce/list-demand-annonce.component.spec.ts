import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDemandAnnonceComponent } from './list-demand-annonce.component';

describe('ListDemandAnnonceComponent', () => {
  let component: ListDemandAnnonceComponent;
  let fixture: ComponentFixture<ListDemandAnnonceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListDemandAnnonceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDemandAnnonceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
