import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AnnonceService } from 'src/app/backendService/annonce.service';
import { AnnonceModel } from 'src/app/modele/annonce';

@Component({
  selector: 'app-annonce-detail',
  templateUrl: './annonce-detail.component.html',
  styleUrls: ['./annonce-detail.component.scss']
})
export class AnnonceDetailComponent implements OnInit {
  id:any;
  annonce: AnnonceModel = new AnnonceModel();
  detail_spinner:boolean = false;
  constructor(
    private _route: ActivatedRoute,
    private AnnonceApi: AnnonceService
  ) { }

  ngOnInit(): void {
    this._route.params.subscribe(
      (params) => {
        this.id = params['id'];
      }
    )
    console.log("the id is: " + this.id);
    this.getSingleAnnonce(this.id);
  }

  public getSingleAnnonce(id:any){
    this.detail_spinner = true;
    this.AnnonceApi.getSingleAnnounce(id).subscribe(
      (resultat:any)=>{
        console.log(resultat);
        let result = resultat;
      if(result.image_1 !== null){
        this.annonce.image_1 = result.image_1;
      }
      if(result.image_2 !== null){
        this.annonce.image_2 = result.image_2;
      }
      if(result.image_3 !== null){
        this.annonce.image_3 = result.image_3;
      }
      if(result.image_4 !== null){
        this.annonce.image_4 = result.image_4;
      }
      if(result.image_5 !== null){
        this.annonce.image_5 = result.image_5;
      }
      if(result.image_6 !== null){
        this.annonce.image_6 = result.image_6;
      }
      this.annonce.house_type = result.house_type;
      this.annonce.price = result.price;
      this.annonce.kitchen_nb = result.kitchen_nb;
      this.annonce.room_nb = result.room_nb;
      this.annonce.toilet_nb = result.toilet_nb;
      this.annonce.description = result.description;
      this.annonce.owner = result.owner;
      this.detail_spinner = false;
      }
    )
  }

}
