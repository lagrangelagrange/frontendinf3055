import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/backendService/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  password = '';
  confirmPassword = '';
  email = '';
  first_name = ''
  city = '';
  student: boolean = false;
  advertiser:boolean = false;
  //spinner
  loginSpinner = false;

  constructor(
    private authApi: AuthService,
    private _router: Router,
    
  ) { }

  ngOnInit(): void {
  }

  register() {
    if (this.password == this.confirmPassword) {
      //TODO execute login process
      this.loginSpinner = true;
      let citySelected = document.getElementById("citySelect") as HTMLSelectElement;
      this.city =  citySelected.value
      let body = {
        password: this.password,
        email:this.email,
        first_name:this.first_name,
        city: this.city,
        student: this.student,
        advertiser: this.advertiser
      }
      console.log(body);
      this.authApi.register(body).subscribe(
        (resultat:any)=>{
          console.log(resultat);
          this.loginSpinner = false;
          this._router.navigate(['/login']);
        }
      ),(err:any)=>{
        console.log("Une erreur est survenue",err);
        this.loginSpinner = false;
      }

    }else{
      alert("Passwords don't match");
    }
  }

}
